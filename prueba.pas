program prueba;

  const
    amountPre = 50000;
    amountPost = 300000;

  var
    prom: Real;
    grado: Integer;
    amountTotal: Real;
    amountDiscount: Real;

begin
  amountTotal:= 0;
  amountDiscount:= 0;
  WriteLn('Promedio: ');
  ReadLn(prom);
  WriteLn('Grado: ');
  WriteLn('Seleccione 1 para pregrado: ');
  WriteLn('Seleccione 2 para postgrado: ');
  ReadLn(grado);
  while (grado <> 1) and (grado <> 2) do
    begin
    WriteLn('Debes ingresar 1 o 2');
    ReadLn(grado);
    end; {while}

  if (prom >= 4.5) and (grado = 1) then
  begin
    amountTotal:= 28 * amountPre;
    amountDiscount:= amountTotal - (amountTotal * 0.25);
  end {if}
  else if (prom >= 4.0) and (prom < 4.5) and (grado = 1) then
  begin
    amountTotal:= 25 * amountPre;
    amountDiscount:= amountTotal - (amountTotal * 0.10);
  end {if}
  else if (prom >= 3.5) and (prom < 4.0) and (grado = 1) then
  begin
    amountTotal:= 20 * amountPre;
    amountDiscount:= amountTotal;
  end
  else if (prom >= 2.5) and (prom < 3.5) and (grado = 1) then
  begin
    amountTotal:= 15 * amountPre;
    amountDiscount:= amountTotal;
  end
  else if (prom < 2.5) and (grado = 1) then
  begin
    WriteLn('No podra matricular');
  end
  else if (prom >= 4.5) and (grado = 2) then
  begin
    amountTotal:= 20 * amountPost;
    amountDiscount:= amountTotal - (amountTotal * 0.20);
  end
  else if (prom < 4.5) and (grado = 2) then
  begin
    amountTotal:= 10 * amountPost;
    amountDiscount:= amountTotal;
  end;

  if amountTotal <> 0 then
    begin                 
    WriteLn('El monto total a pagar es: ', amountTotal:5:2);
    WriteLn('El monto total a pagar con descuento es: ', amountDiscount:5:2);
    end; {if}

  {readln;}
end.